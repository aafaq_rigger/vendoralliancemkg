@extends('layouts.main')
@section('title', 'Generate Report' )
@section('content')
    <div class="main-content" style="margin-bottom: 20px !important;">
        <div class="row">
            <div class="col-md-6">
                <h5>Generate Report</h5>
            </div>

        </div>
        <hr>
        <form method="post" action="{{ route('generate_report') }}">
            @csrf
            <div class="form-row">

                <div class="form-group col-md-3">
                    <label for="name">Select Range: </label>
                    <input class="form-control" type="text" data-toggle="daterangepicker" maxlength="23" name="date" data-filter-type="date-range">

                </div>

                <div class="form-group col-md-2">
                    <button style="margin-top: 25px;" type="submit" class="btn btn-primary apply">@lang('form.apply')</button>
                </div>

            </div>

        </form>
    </div>
@endsection

@section('onPageJs')


    <script>

        $(function () {

            $('[data-filter-type="date-range"]').daterangepicker({
                showDropdowns: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'This week': [moment().startOf('week'), moment().endOf('week')],
                    'Last week': [moment().subtract(6, 'days'), moment()],
                    'Last 2 weeks': [moment().subtract(13, 'days'), moment()],
                    'This month': [moment().startOf('month'), moment().endOf('month')],
                    'Last month': [moment().subtract(1, 'month').startOf('month'),
                        moment().subtract(1, 'month').endOf('month')]
                },
                autoUpdateInput: true,
                applyClass: 'btn-sm btn-primary',
                cancelClass: 'btn-sm btn-default',
                locale: {
                    format: 'YYYY-MM-DD',
                    applyLabel: 'Apply',
                    cancelLabel: 'Clean',
                    fromLabel: 'Since',
                    toLabel: 'Until',
                    customRangeLabel: 'Select range',
                    daysOfWeek: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June',
                        'July', 'August', 'September', 'October', 'November',
                        'December'],
                    firstDay: 1
                }
            });

        });

    </script>
    @yield('innerPageJs')
@endsection
