@extends('layouts.main')
@section('title', 'Check Report' )
@section('content')
<div class="row">
    <a href="/reports/report" class="btn ml-3 mr-3 btn-primary"><i class="fas fa-arrow-left"></i>  Go Back</a>
    <form class="form-inline" action="report/download" name="download_report" method="post">
        @csrf
        <input type="hidden" name="from" value="{{ $from }}">
        <input type="hidden" name="to" value="{{ $to }}">
        <input type="hidden" name="sales" value="{{ $sales }}">
        <input type="hidden" name="expenses" value="{{ $expenses }}">

        <input class="btn btn-success" value="Downlaod report" type="submit">
    </form>
{{--    <div class="ml-2">--}}
{{--        <input class="btn btn-primary" type="button" onclick="tableToExcel('divDataHolder', 'W3C Example Table')" value="Export to Excel">--}}
{{--    </div>--}}
</div>
<br>
    <div id="divDataHolder" class="main-content" style="margin-bottom: 20px !important;">
        <br><br>
        <div class="text-center">
            <h3>Vendor Alliance Marketing Inc.</h3>
            <p>PO Box 870109, Stone Mountain, GA 30087</p>
        </div>
    <div class="text-center">
        <h3>Profit and Loss Statement</h3>
        <p>for the period {{ $from }} to {{ $to }}</p>
    </div>
        <br>
        <div class="container">
        <div class="pb-2">
            <h5><b><u>Expenses:</u></b></h5>
        </div>
        <div class="row">
            <div class="col-lg-6">
                Total Expenses:
            </div>
            <div class="col-lg-6 text-right">
                @isset($expenses)
                    {{ $expenses }}$
                @endisset
            </div>
        </div>
            <br>
            <div class="pb-2">
                <h5><b><u>Sales:</u></b></h5>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    Total Sales:
                </div>
                <div class="col-lg-6 text-right">
                    @isset($sales)
                        {{ $sales }}$
                    @endisset
                </div>
            </div>
            <br>
            <div class="pb-2">
                <h5><b><u>Profit/Loss:</u></b></h5>
            </div>
            <div class="row">
                @if($sales - $expenses > 0)
                    <div class="col-lg-6">
                        Total Profit:
                    </div>
                    <div class="col-lg-6 text-right">
                        <b>{{ $sales - $expenses }}$</b>
                    </div>
                @else
                    <div class="col-lg-6">
                        Total Loss:
                    </div>
                    <div class="col-lg-6 text-right">
                        <b> - {{ $expenses - $sales }}$</b>
                    </div>
                @endif
            </div>
            <br>

            <div class="row">
                    @if($expenses == 0 & $sales == 0)
                        <div class="text-center"><b>No Profit, No Loss!</b></div>
                    @elseif($expenses == 0)
                        <div class="col-lg-6">
                            Loss in Percentage:
                        </div>
                        <div class="col-lg-6 text-right">
                                <b>100%</b>
                        </div>
                    @else
                        @if($diff_sme > 0)
                            <div class="col-lg-6">
                                Profit in Percentage:
                            </div>
                            <div class="col-lg-6 text-right">
                                <b>{{ $diff_sme / $expenses * 100 }}%</b>
                            </div>
                        @else
                            <div class="col-lg-6">
                                Loss in Percentage:
                            </div>
                            <div class="col-lg-6 text-right">
                                <b>{{ $diff_sme / $expenses * 100 }}%</b>
                            </div>
                        @endif
                    @endif
            </div>
        </div>

            <br>

{{--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
{{--        <script type="text/javascript">--}}
{{--            var tableToExcel = (function() {--}}
{{--                var uri = 'data:application/vnd.ms-excel;base64,'--}}
{{--                    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'--}}
{{--                    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }--}}
{{--                    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }--}}
{{--                return function(table, name) {--}}
{{--                    if (!table.nodeType) table = document.getElementById(table)--}}
{{--                    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}--}}
{{--                    window.location.href = uri + base64(format(template, ctx))--}}
{{--                }--}}
{{--            })()--}}
{{--        </script>--}}


    </div>
@endsection

