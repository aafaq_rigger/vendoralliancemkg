<html>
    <head>
        <title>Profit and Loss Statement</title>
    </head>
    <body>
    <div class="main-content" style="margin-bottom: 20px !important;">
        <div style="text-align: center">
            <h3>Vendor Alliance Marketing Inc.</h3>
            <p>PO Box 870109, Stone Mountain, GA 30087</p>
        </div>
        <div style="text-align: center">
            <h3>Profit and Loss Statement</h3>
            <p>for the period {{ $from }} to {{ $to }} </p>
        </div>
        <h4><b><u>Expenses:</u></b></h4>
        <div style="width:100%;">
            <div style="">
                Total Expenses :
            </div>
            <div style="text-align: right">
                {{ $expenses }}$
            </div>
        </div>
        <h4><b><u>Sales:</u></b></h4>
        <div style="width:100%;">
            <div style="">
                Total Sales :
            </div>
            <div style="text-align: right">
                {{ $sales }}$
            </div>
        </div>
        <h4><b><u>Profit/Loss:</u></b></h4>

        <div style="width:100%">
            @if($sales - $expenses > 0)
                <div style="">
                    Total Profit:
                </div>
                <div style="text-align: right">
                    <b>
                    {{ $sales - $expenses }}$
                    </b>
                </div>
            @else
                <div >
                    Total Loss:
                </div>
                <div style="text-align: right">
                        <b>- {{ $expenses - $sales }}$</b>
                </div>
            @endif
        </div>
        <div style="width:100%;">
                @if($expenses == 0 & $sales == 0)
                    <div style="text-align: center"><b>No Profit, No Loss!</b></div>
                @elseif($expenses == 0)
                    <div>
                        Profit in Percentage:
                    </div>
                    <div style="text-align: right">
                            100%
                    </div>
                @else
                    @if($diff_sme > 0)
                    <div>
                        Profit in Percentage:
                    </div>
                    <div style="text-align: right">
                        <b>
                            {{ $diff_sme / $expenses * 100 }}%
                        </b>
                    </div>
                    @else
                        <div >
                            Loss in Percentage:
                        </div>
                        <div style="text-align: right">
                            <b>
                                {{ $diff_sme / $expenses * 100 }}%
                            </b>
                        </div>
                    @endif
                @endif
        </div>
        <br>
    </div>
    </body>
</html>




