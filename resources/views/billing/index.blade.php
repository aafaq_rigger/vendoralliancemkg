@extends('layouts.main')
@section('title', 'Billing Statement' )
@section('content')
    <div class="main-content" style="margin-bottom: 20px !important;">
        <div class="row">
            <div class="col-md-6">
                <h5>Billing Report</h5>
            </div>

        </div>
        <hr>

        <form action="{{ route('generate_billing_statement') }}" method="post">
        @csrf
            <div class="form-row">
                <div class="form-group col-md-2">
                    <label for="customer"><b>@lang('form.customer')</b></label>
                    <select class="form-control" name="customer" id="">
                        <option>- Select Customer -</option>
                        @foreach($customers as $customer)
                            <option value="{{ $customer->id }}">{{ $customer->name }}</option>
                        @endforeach
                    </select>

                </div>
                <div class="form-group col-md-2">
                    <label for="month"><b>Month</b></label>
                    <select class="form-control" name="month" id="">
                        <option>- Month -</option>
                        <option value="01">January</option>
                        <option value="02">Febuary</option>
                        <option value="03">March</option>
                        <option value="04">April</option>
                        <option value="05">May</option>
                        <option value="06">June</option>
                        <option value="07">July</option>
                        <option value="08">August</option>
                        <option value="09">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <label for="year"><b>Year</b></label>
                    <select class="form-control" name="year" id="">
                        <option>- Year -</option>
                        <option value="2020">2020</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                        <option value="2015">2015</option>
                        <option value="2014">2014</option>
                        <option value="2013">2013</option>
                        <option value="2012">2012</option>
                        <option value="2011">2011</option>
                        <option value="2010">2010</option>
                        <option value="2009">2009</option>
                        <option value="2008">2008</option>
                        <option value="2007">2007</option>
                        <option value="2006">2006</option>
                        <option value="2005">2005</option>
                        <option value="2004">2004</option>
                        <option value="2003">2003</option>
                        <option value="2001">2002</option>
                        <option value="2000">2002</option>
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <input type="submit" style="margin-top: 29px" class="btn btn-primary" value="Generate">
                </div>
            </div>
        </form>
    </div>
@endsection
