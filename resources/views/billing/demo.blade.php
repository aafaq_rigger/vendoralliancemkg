@extends('layouts.main')
@section('title', 'Monthly Statement')
@section('content')
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Monthly Statement</title>
        <style>
        .grey{
            background: #6f6a6a;
            color: white;
        }
        .light-grey{
            background:#b7b7b7;
        }
        .night-grey{
            background: #dedede;
        }
        </style>
</head>
<body id="testTable" class="">
<div class="row">
    <a href="{{ url()->previous() }}" class="btn ml-3 btn-primary"><i class="fas fa-arrow-left"></i>  Go Back</a>
    <div class="ml-2">
        <form action="{{ route('billing_statement_pdf', $customer->id) }}" method="post">
            @csrf
            <input type="hidden" name="month" value="{{ $month }}">
            <input type="hidden" name="year" value="{{ $year }}">
            <input type="submit" value="Export as PDF" class="btn btn-success">
        </form>
    </div>
</div>
<div id="divDataHolder" class="bg-white container border p-lg-5 m-2 mb-5 mt-5">
<div class="text-center">
    <h1>Vendor Alliance Marketing Inc.</h1>
    <hr>
</div>
<div class="row">
    <div class="col-4 center left">
        <img class="text-center" src="{{asset('storage/uploads/logo_internal_125x34.png')}}" width="100px" height="100px" alt="Logo">
        <p><span><b>Address: </b></span>PO Box 870109, Stone Mountain, GA 30087</p>
    </div>
    <div class="col-4"></div>
    <div class="col-4 right">
        <p>Fax Number: </p>
        <p>(678) 531-0075</p>
        <p>billing@vendoralliancemkg.com</p>
    </div>
</div>
<br>
<div class="text-center">
    <hr>
    <h1>Monthly Statement</h1>
    <hr>
</div>
<div class="row">
    <div class="col-4 left">
        <p><span><b>Date: </b></span>@php echo date('Y-m-d') @endphp</p>
        <p><span><b>Customer ID: </b></span>{{ $customer->number }}</p>
    </div>
    <div class="col-4"></div>
    <div class="col-4 right">
        <p>{{ $customer->shipping_address }}</p>
        <p>{{ $customer->shipping_state .', '.$customer->shipping_zip_code }}</p>
    </div>
</div>
<div class="row">
    @foreach ($invoices as $invoice)
        <p style="display: none">{{ $total  += ($invoice->total - $invoice->amount_paid) }}</p>
    @endforeach

    <p class="col-4 left grey p-2 center">Total Balance:</p>
    <p class="col-8 right light-grey p-2">{{$total}}$</p>
</div>
<br>
<br>
<table class="table table-hover table-responsive">
    <tr class="table-bordered">
        <th class="grey pd10">Invoice Date</th>
        <th class="grey pd10">Due Date</th>
        <th class="grey pd10">PO #</th>
        <th class="grey pd10">Unit</th>
        <th class="grey pd10">Description</th>
        <th class="grey pd10">Current</th>
        <th class="grey pd10">31 - 45</th>
        <th class="grey pd10">46 - 60</th>
        <th class="grey pd10" style="color: red">Over 60</th>
        <th class="grey pd10">Payment</th>
        <th class="grey pd10">Balance</th>
    </tr>
    @foreach($invoices as $invoice)

    <tr class="table-bordered">
        <td class="td pd10">{{ $invoice->date }}</td>
        <td class="td pd10">{{ $invoice->due_date }}</td>
        <td class="td pd10">{{ $invoice->number }}</td>
        <td class="td pd10">
            @foreach($invoice_items as $invoice_item)
            @if($invoice->id == $invoice_item->invoice_id)
                {{ $invoice_item->description.', ' }}<br>
            @endif
            @endforeach
        </td>
        <td class="td pd10">
            @foreach($invoice_items as $invoice_item)
                @if($invoice->id == $invoice_item->invoice_id)
                    {{ $invoice_item->long_description.', ' }}<br>
                @endif
            @endforeach
        </td>
        @if($days[$invoice->id] == 30 or $days[$invoice->id] < 30)
            <td class="td pd10">{{ $invoice->total }}</td>
        @else
            <td class="td pd10"></td>
        @endif
        @if($days[$invoice->id] > 31 and $days[$invoice->id] < 45)
            <td class="td pd10">{{ $invoice->total }}</td>
        @else
            <td class="td pd10"></td>
        @endif
        @if($days[$invoice->id] > 45 and $days[$invoice->id] < 60)
            <td class="td pd10">{{ $invoice->total }}</td>
        @else
            <td class="td pd10"></td>
        @endif
        @if($days[$invoice->id] > 60)
            <td class="td pd10">{{ $invoice->total }}</td>
        @else
            <td class="td pd10"></td>
        @endif
        <td class="td pd10">{{$invoice->amount_paid}}</td>
        <td class="td pd10 light-grey">{{ $invoice->total - $invoice->amount_paid }}</td>
    </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="td pd10">Total Balance:</td>
        <td class="table-bordered night-grey">
            @foreach ($invoices as $invoice)
                <p style="display: none">{{ $balance += ($invoice->total - $invoice->amount_paid) }}</p>
            @endforeach
            <p><b>$ {{$balance}}</b></p>
        </td>
    </tr>
</table>
<div class="border-bottom">
    <p><span><b>Reminder: </b></span>Please make check available in vendor alliance marketing Inc.</p>
    <p><span><b>Terms: </b></span>
        @foreach($invoices as $invoice)
            {{ $invoice->terms_and_condition}}<br>
        @endforeach
    </p>
</div>
<br><br>
<h3 class="col-lg-12 p-md-2 grey">Remitance</h3>
<table style="width: 100%" class="table table-hover">
    <tr>
        <td>Customer Name:</td>
        <td>{{ $customer->name }}</td>
    </tr>
    <tr>
        <td>Customer ID:</td>
        <td>{{ $customer->number }}</td>
    </tr>
    <tr>
        <td>Statement Month:</td>
        <td>{{ date('F',$month) }}</td>
    </tr>
    <tr>
        <td>Date: </td>
        <td>{{ date('Y-m-d') }}</td>
    </tr>
    <tr>
        <td>Amount Due: </td>
        <td>$ {{ $balance }}</td>
    </tr>
    <tr>
        <td>Amount Enclosed: </td>
        <td></td>
    </tr>
</table>
</div>

{{--<script type="text/javascript">--}}
{{--    var tableToExcel = (function() {--}}
{{--        var uri = 'data:application/vnd.ms-excel;base64,'--}}
{{--            , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'--}}
{{--            , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }--}}
{{--            , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }--}}
{{--        return function(table, name) {--}}
{{--            if (!table.nodeType) table = document.getElementById(table)--}}
{{--            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}--}}
{{--            window.location.href = uri + base64(format(template, ctx))--}}
{{--        }--}}
{{--    })()--}}
{{--</script>--}}
</body>

@endsection