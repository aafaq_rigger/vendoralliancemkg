<html lang="en">
<head>
    <title>Monthly Statement</title>
    <style>
        .main-body{
            font-family:ArialMT;
            padding: 1% 3%;
            border: 2px solid black;
            width:57%;
            margin-left: 20%;
            margin-right: 20%;
        }

        .pd10{
            padding:10px;
        }
        .center{
            text-align: center;
        }
        .row{
            width:100%;
            overflow: hidden;
        }
        .right{
            float:right;
        }
        .left{
            float:left;
        }
        .col-6{
            width:50%
        }
        .col-4{
            width:33.3333%
        }
        .col-8{
            width:60.7777%;
        }
        .col-m{
            width: 63.36%;
        }
        .grey{
            background: #6f6a6a;
            color: white;
        }
        .light-grey{
            background:#b7b7b7;
        }
        .night-grey{
            background: #dedede;
        }
        .red{
            color:red;
        }
        .border{
            border: 1px solid black;
        }
        .border-bottom{
            border-bottom:1px dotted black;
        }
        .td{
            border: 1px solid black;
        }
    </style>
</head>
<body>
<div class="center">
    <h1>Vendor Alliance Marketing Inc.</h1>
    <hr>
</div>
<div class="row">
    <div class="col-4 center left">
       <img src="{{asset('storage/uploads/logo_internal_125x34.png')}}" width="100px" height="100px" alt="Logo">
        <p><span><b>Address: </b></span>PO Box 870109, Stone Mountain, GA 30087</p>
    </div>
    <div class="col-4"></div>
    <div class="col-4 right">
        <p>Fax Number: </p>
        <p>(678) 531-0075</p>
        <p>billing@vendoralliancemkg.com</p>
    </div>
</div>
<br>
<div class="center">
    <hr>
    <h1>Monthly Statement</h1>
    <hr>
</div>
<div class="row">
    <div class="col-4 left">
        <p><span><b>Date: </b></span>@php echo date('Y-m-d') @endphp</p>
        <p><span><b>Customer ID: </b></span>{{ $customer->number }}</p>
    </div>
    <div class="col-4"></div>
    <div class="col-4 right">
        <p>{{ $customer->shipping_address }}</p>
        <p>{{ $customer->shipping_state .', '.$customer->shipping_zip_code }}</p>
    </div>
</div>
<div class="row">
    @foreach ($invoices as $invoice)
        <p style="display: none">{{ $total += ($invoice->total - $invoice->amount_paid) }}</p>
    @endforeach

    <p class="col-4 left grey pd10">Total Balance:</p>
    <p class="col-8 right light-grey pd10" style="margin-top: 0px">{{$total}}$</p>
</div>
<br>
<br>
<table class="row border">
    <tr>
        <th class="grey pd10">Invoice Date</th>
        <th class="grey pd10">Due Date</th>
        <th class="grey pd10">PO #</th>
        <th class="grey pd10">Unit</th>
        <th class="grey pd10">Description</th>
        <th class="grey pd10">Current</th>
        <th class="grey pd10">31 - 45</th>
        <th class="grey pd10">46 - 60</th>
        <th class="grey pd10 red">Over 60</th>
        <th class="grey pd10">Payment</th>
        <th class="grey pd10">Balance</th>
    </tr>
    @foreach($invoices as $invoice)
        <tr class="table-bordered">
            <td class="td pd10">{{ $invoice->date }}</td>
            <td class="td pd10">{{ $invoice->due_date }}</td>
            <td class="td pd10">{{ $invoice->number }}</td>
            <td class="td pd10">
                @foreach($invoice_items as $invoice_item)
                    @if($invoice->id == $invoice_item->invoice_id)
                        {{ $invoice_item->description.', ' }}<br>
                    @endif
                @endforeach
            </td>
            <td class="td pd10">
                @foreach($invoice_items as $invoice_item)
                    @if($invoice->id == $invoice_item->invoice_id)
                        {{ $invoice_item->long_description.', ' }}<br>
                    @endif
                @endforeach
            </td>
            @if($days[$invoice->id] == 30 or $days[$invoice->id] < 30)
                <td class="td pd10">{{ $invoice->total }}</td>
            @else
                <td class="td pd10"></td>
            @endif
            @if($days[$invoice->id] > 31 and $days[$invoice->id] < 45)
                <td class="td pd10">{{ $invoice->total }}</td>
            @else
                <td class="td pd10"></td>
            @endif
            @if($days[$invoice->id] > 45 and $days[$invoice->id] < 60)
                <td class="td pd10">{{ $invoice->total }}</td>
            @else
                <td class="td pd10"></td>
            @endif
            @if($days[$invoice->id] > 60)
                <td class="td pd10">{{ $invoice->total }}</td>
            @else
                <td class="td pd10"></td>
            @endif
            <td class="td pd10">{{ $invoice->amount_paid }}</td>
            <td class="td pd10 light-grey">{{ $invoice->total - $invoice->amount_paid }}</td>
        </tr>
    @endforeach
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="td pd10">Total Balance:</td>
        <td class="table-bordered night-grey">
            @foreach ($invoices as $invoice)
                <p style="display: none">{{ $balance += ($invoice->total - $invoice->amount_paid) }}</p>
            @endforeach
            <p><b>$ {{$balance}}</b></p>
        </td>
    </tr>
</table>
<div class="border-bottom">
    <p><span><b>Reminder: </b></span>Please make check available in vendor alliance marketing Inc.</p>
    <p><span><b>Terms: </b></span>
        @foreach($invoices as $invoice)
            {{ $invoice->terms_and_condition}}<br>
        @endforeach
    </p>
</div>
<br><br>
<div class="row">
    <div class="row grey pd10">Remittance</div>
</div>
<table style="width: 100%" class="table table-hover">
    <tr >
        <td>Customer Name:</td>
        <td>{{ $customer->name }}</td>
    </tr>
    <tr>
        <td>Customer ID:</td>
        <td>{{ $customer->number }}</td>
    </tr>
    <tr>
        <td>Statement Month:</td>
        <td>{{ date('F',$month) }}</td>
    </tr>
    <tr>
        <td>Date: </td>
        <td>{{ date('Y-m-d') }}</td>
    </tr>
    <tr>
        <td>Amount Due: </td>
        <td>$ {{ $balance }}</td>
    </tr>
    <tr>
        <td>Amount Enclosed: </td>
        <td></td>
    </tr>
</table>
</body>
</html>