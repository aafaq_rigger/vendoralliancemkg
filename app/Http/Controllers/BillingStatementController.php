<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use App\Customer;
use App\Invoice;
use App\InvoiceItem;
use App\Services\Pdf;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
//use Illuminate\Support\Facades\App;

class BillingStatementController extends Controller
{
    function index(){
        $customers = Customer::all();
        return view('billing.index',compact('customers'));
    }

    function generate_billing_statement(Request $request){

        $customer_id = $request->customer;
        $month = $request->month;
        $year = $request->year;
        $customer = Customer::where('id', $customer_id)->first();
        $invoices = Invoice::where('customer_id',$customer_id)
        ->whereYear('date', $year)
        ->get();
        $data = array();

        foreach ($invoices as $invoice){
            $due = Carbon::parse($invoice->due_date);
            $now = Carbon::parse($year.'-'.$month)->endOfMonth();
            $data[$invoice->id] = $now->diffInDays($due);
        }

        $invoice_items = InvoiceItem::all();

        $total = 0;
        $balance = 0;
        return view('billing.demo', ['month'=>$month,'year'=>$year,'customer'=>$customer,'invoices'=>$invoices,'days'=>$data,'balance'=>$balance,'total'=>$total,'invoice_items'=>$invoice_items]);
    }


    function billing_statement_pdf(Request $request){

        $customer_id = $request->id;
        $month = $request->month;
        $year = $request->year;
        $customer = Customer::where('id', $customer_id)->first();
        $invoices = Invoice::where('customer_id',$customer_id)
            ->whereYear('date', $year)
            ->get();        $data = array();

        foreach ($invoices as $invoice){
            $due = Carbon::parse($invoice->due_date);
            $now = Carbon::parse($year.'-'.$month)->endOfMonth();
            $data[$invoice->id] = $now->diffInDays($due);
        }

        $invoice_items = InvoiceItem::all();
        $total = 0;
        $balance = 0;
        $html = view('billing.statement', ['month'=>$month,'customer'=>$customer,'invoices'=>$invoices,'days'=>$data,'balance'=>$balance,'total'=>$total,'invoice_items'=>$invoice_items])->render();

        $pdf = new Pdf([
            'orientation' => 'L']);

        $pdf->download($html, 'Monthly Statement');

    }
}
